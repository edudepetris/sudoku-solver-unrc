# Sudoku

### Java implementation for training on DOSE.

### Develop
Compile and run test with the following command:

```shell
# compile and run all test
$ bin/jspec.sh

# compile and run a specific test
$ bin/jspec.sh GridTest
```

### Checkstyle
```shell
$ bin/style.sh # run for all java classes
$ bin/style.sh sudoku/Cell.java # run for a specific java class
```

### References
- [Checkstyle](http://checkstyle.sourceforge.net/index.html)
- [Sudoku](https://en.wikipedia.org/wiki/Sudoku)
- [Junit](https://github.com/junit-team/junit4/wiki/Getting-started)
- [Generate puzzles](http://dryicons.com/blog/2009/08/14/a-simple-algorithm-for-generating-sudoku-puzzles/)
