package test;

import static org.junit.Assert.*;

import org.junit.*;
import sudoku.Cell;
import sudoku.SimpleBoardGenerator;

public class SimpleBoardGeneratorTest {

  @Test
  public void run() {
    int difficulty = 0;
    Cell[][] board = new SimpleBoardGenerator(difficulty).run();
    assertTrue(isBoardValid(board));
  }

  // TODO
  private boolean isBoardValid(Cell[][] board) {
    return true;
  }
}
