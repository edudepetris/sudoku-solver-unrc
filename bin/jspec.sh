#!/bin/bash

# compile project
javac -cp .:./lib/junit.jar:./lib/hamcrest-core-1.3.jar sudoku/*.java test/*.java

path=$1

if [[ -n "$path" ]]; then
  java -cp .:./lib/junit.jar:./lib/hamcrest-core-1.3.jar org.junit.runner.JUnitCore 'test.'$path
else
  for file in test/*.java; do
    name+=' test.'$(echo $file | cut -f 1 -d '.' | cut -f 2 -d '/')
  done
  java -cp .:./lib/junit.jar:./lib/hamcrest-core-1.3.jar org.junit.runner.JUnitCore $name
fi