#!/bin/bash
path=$1

if [[ -n "$path" ]]; then
  java -jar lib/checkstyle-backport-jre6-8.0-all.jar -c google_checks.xml $path
else
  java -jar lib/checkstyle-backport-jre6-8.0-all.jar -c google_checks.xml sudoku/*.java
fi
