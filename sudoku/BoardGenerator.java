package sudoku;

import sudoku.Cell;

public interface BoardGenerator {
  public Cell[][] run();
}